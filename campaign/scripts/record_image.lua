
function onInit()
	UDG5ESaveOverlayHelper.verbose({"record_image.lua::oninit()"});
	super.onInit();

	update();

	OptionsManager.registerCallback("FIVEE_SAVE_OVERLAY_VISIBILITY", visibilitySettingUpdated);
end

function visibilitySettingUpdated()
	UDG5ESaveOverlayHelper.verbose({"record_image.lua::visibilitySettingUpdated()"});

	updateVisibility();
end

function update()
	UDG5ESaveOverlayHelper.verbose({"record_image.lua::update()"});
	super.update();

	updateVisibility();
end

function updateVisibility()
	UDG5ESaveOverlayHelper.verbose({"record_image.lua::updateVisibility()"});

	local image = getImage();
	local bHasTokens = image.hasTokens();

	local visibleToAll = OptionsManager.isOption("FIVEE_SAVE_OVERLAY_VISIBILITY", "all");
	local showButton = Session.IsHost or visibleToAll;

	if showButton and bHasTokens then
		toolbar_clear_saves.setVisible(true);
		clear_save_separator.setVisible(true);
	else
		toolbar_clear_saves.setVisible(false);
		clear_save_separator.setVisible(false);

		UDG5ESaveOverlaySaveOverlayManager.deleteSaveOverlays();
	end
end


function onClose()
	UDG5ESaveOverlayHelper.verbose({"record_image.lua::onClose()"});

	OptionsManager.unregisterCallback("FIVEE_SAVE_OVERLAY_VISIBILITY", visibilitySettingUpdated);
end
