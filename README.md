# 5E Save Overlay for FGU
A little diddy to toss out an icon overlay based on the most current saving throw result

[Download](https://gitlab.com/ccthiel/5e-save-overlay/-/jobs/artifacts/latest/file/5E-Save-Overlay.ext?job=build-extension) the latest version of the plugin!

[Gitlab repository](https://gitlab.com/ccthiel/5e-save-overlay)

[Fantasy Grounds Forum Thread](https://www.fantasygrounds.com/forums/showthread.php?66438-5E-Save-Oerlay&p=582141)

[Discord Channel](https://discord.com/channels/812870526455250945/814253849124274236)

Proud member of the [Fantasy Grounds Unofficial Developer's Guild!](https://gitlab.com/fantasy-grounds-unofficial-developers-guild)

Features:
  * All: Save overlays for tokens based upon last saving throw roll
  * Menu option as to whether or not overlays are visible to GM or everyone
  * Menu option as to whether or not overlays are reset on combat turn end
  * Chat command /clearSaveOverlays to clear overlays for everyone

Please see [Bug Reporting](https://gitlab.com/ccthiel/5e-save-overlay/-/blob/latest/BUG_REPORT.md) when it comes to filing a bug. I'd absolutely weclome people reporting bugs (or even forking the repository and submitting a merge request!), but please do a bit of work on your part to make my life easier. Thank you! 

Want to become a member of the Fantasy Grounds Unofficial Developers Guild? [Join up!](https://discord.gg/yAXPgR8Bc8) It'll give you [developer](https://docs.gitlab.com/ee/user/permissions.html) access to this repository (and hopefully others soon!)
