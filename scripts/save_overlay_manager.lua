OOB_MSGTYPE_DELETE_ALL_SAVE_OVERLAYS = "delete_all_save_overlays";
OOB_MSGTYPE_APPLY_SAVE_OVERLAYS = "apply_save_overlays";
function onInit()
	originalOnTurnEndEvent = CombatManager.onTurnEndEvent
	CombatManager.onTurnEndEvent = onTurnEndEvent;

    originalApplySave = ActionSave.applySave
    ActionSave.applySave = applySave

	if Session.IsHost then
		Comm.registerSlashHandler("clearSaveOverlays", sendMessageDeleteAllSaveOverlays);
	end

	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_DELETE_ALL_SAVE_OVERLAYS, handleMessageDeleteAllSaveOverlays);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLY_SAVE_OVERLAYS, handleMessageApplySaveOverlays);
end

function applySave(rSource, rOrigin, rAction, sUser)
	UDG5ESaveOverlayHelper.verbose({"save_overlay_manager.lua::applySave(rSource, rOrigin, rAction, sUser)", "rSource:", rSource, "rOrigin:", rOrigin, "rAction:", rAction, "sUser:", sUser});

    originalApplySave(rSource, rOrigin, rAction, sUser)

	if not rOrigin then
		return;
	end

	halfOnSave = (rAction.sSaveDesc:match("%[HALF ON SAVE%]") ~= nil);

	if rAction.nTarget <= 0 then
		return
	end

	local saveIcon = nil;
	local avoidDamage = false;

	if EffectManager5E.hasEffectCondition(rSource, "Avoidance") then
		avoidDamage = true;
	elseif EffectManager5E.hasEffectCondition(rSource, "Evasion") then
		local sSave = rAction.sDesc:match("%[SAVE%] (%w+)");
		if sSave then
			sSave = sSave:lower();
		end
		if sSave == "dexterity" then
			avoidDamage = true;
		end
	end

	if rAction.nTotal >= rAction.nTarget then
		saveIcon = "save_success";
	else
		if halfOnSave and avoidDamage then
			saveIcon = "save_partial";
		else
			saveIcon = "save_fail";
		end
	end

	local nodeCT = CombatManager.getCTFromNode(rSource.sCTNode);

	local tokenRefNode = DB.getValue(nodeCT, "tokenrefnode", "");
	local tokenRefID = DB.getValue(nodeCT, "tokenrefid", "");
	local nDU = GameSystem.getDistanceUnitsPerGrid();
	local tokenSpace = DB.getValue(nodeCT, "space",  nDU);

	local saveOverlayVisibility = OptionsManager.getOption("FIVEE_SAVE_OVERLAY_VISIBILITY");

	if saveOverlayVisibility == "all" then
		sendMessageApplySaveOverlays(tokenRefNode, tokenRefID, saveIcon, nDU, tokenSpace);
	else
		updateSaveOverlay(tokenRefNode, tokenRefID, saveIcon, nDU, tokenSpace);
	end
end

function updateSaveOverlay(tokenRefNode, tokenRefID, saveIcon, nDU, tokenSpace)
	UDG5ESaveOverlayHelper.verbose({"save_overlay_manager.lua::updateSaveOverlay(tokenRefNode, tokenRefID, saveIcon, nDU, tokenSpace)", "tokenRefNode:", tokenRefNode, "tokenRefID:", tokenRefID, "saveIcon:", saveIcon, "nDU:", nDU, "tokenSpace:", tokenSpace});

	local tokenCT = Token.getToken(tokenRefNode, tokenRefID);

	if tokenCT then
		local nDU = GameSystem.getDistanceUnitsPerGrid();
		local nSpace = math.ceil(tokenSpace / nDU)*100;
		local tokenWidth = nSpace;
		local tokenHeight = nSpace;

		saveWidget = tokenCT.findWidget("save");
		if saveWidget then saveWidget.destroy(); end

		saveWidget = tokenCT.addBitmapWidget();
		saveWidget.setName("save");
		saveWidget.setSize(math.floor(tokenWidth*1), math.floor(tokenHeight*1)); 
		saveWidget.setBitmap(saveIcon);
		saveWidget.bringToFront();
	else
		UDG5ESaveOverlayHelper.verbose({"No token associated with tokenRefID", tokenRefID});
	end
end

function sendMessageDeleteAllSaveOverlays()
	UDG5ESaveOverlayHelper.verbose({"save_overlay_manager.lua::sendMessageDeleteAllSaveOverlays()"});
	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_DELETE_ALL_SAVE_OVERLAYS;
	Comm.deliverOOBMessage(msgOOB);
end

function handleMessageDeleteAllSaveOverlays(msgOOB)
	UDG5ESaveOverlayHelper.verbose({"save_overlay_manager.lua::handleMessageDeleteAllSaveOverlays(msgOOB)", "msgOOB:", msgOOB});

	deleteSaveOverlays();
end

function deleteSaveOverlays()
	UDG5ESaveOverlayHelper.verbose({"save_overlay_manager.lua::deleteSaveOverlays()"});
	local aEntries = CombatManager.getSortedCombatantList();

	for _, node in ipairs(aEntries) do
		local token = CombatManager.getTokenFromCT(node);

		if token then
			saveWidget = token.findWidget("save");
			if saveWidget then 
				saveWidget.setVisible(false);
				saveWidget.destroy(); 
			end
		end
	end
end

function sendMessageApplySaveOverlays(tokenRefNode, tokenRefID, saveIcon, nDU, tokenSpace)
	UDG5ESaveOverlayHelper.verbose({"save_overlay_manager.lua::sendMessageApplySaveOverlays(tokenRefNode, tokenRefID, saveIcon, nDU, tokenSpace)", "tokenRefNode:", tokenRefNode, "tokenRefID:", tokenRefID, "saveIcon:", saveIcon, "nDU:", nDU, "tokenSpace:", tokenSpace});

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLY_SAVE_OVERLAYS;
	msgOOB["tokenRefNode"] = tokenRefNode;
	msgOOB["tokenRefID"] = tokenRefID;
	msgOOB["saveIcon"] = saveIcon;
	msgOOB["nDU"] = nDU;
	msgOOB["tokenSpace"] = tokenSpace;


	UDG5ESaveOverlayHelper.debug("msgOOB", msgOOB);
	Comm.deliverOOBMessage(msgOOB);
end

function handleMessageApplySaveOverlays(msgOOB)
	UDG5ESaveOverlayHelper.verbose({"save_overlay_manager.lua::handleMessageApplySaveOverlays(msgOOB)", "msgOOB:", msgOOB});

	local tokenRefNode = msgOOB["tokenRefNode"];
	local tokenRefID = msgOOB["tokenRefID"];
	local saveIcon = msgOOB["saveIcon"];
	local nDU = msgOOB["nDU"];
	local tokenSpace = msgOOB["tokenSpace"];

	updateSaveOverlay(tokenRefNode, tokenRefID, saveIcon, nDU, tokenSpace);
end

function onTurnEndEvent(nodeCT)
	UDG5ESaveOverlayHelper.verbose({"save_overlay_manager.lua::onTurnEnd(nodeCT)", "nodeCT:", nodeCT});

	resetOnTurnEnd = OptionsManager.getOption("FIVEE_SAVE_OVERLAY_RESET_ON_TURN_END");
	if resetOnTurnEnd == "on" then
		sendMessageDeleteAllSaveOverlays();
	end

	originalOnTurnEndEvent(nodeCT);
end
