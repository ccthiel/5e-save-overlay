function onInit()		
	UDG5ESaveOverlayHelper.verbose({"register_menu_items.lua::onInit()"});
	registerMenuItems();		
end

function registerMenuItems() 	
	UDG5ESaveOverlayHelper.verbose({"register_menu_items.lua::registerMenuItems()"});

	OptionsManager.registerOption2("FIVEE_SAVE_OVERLAY_DEBUG", false, "menu_option_header_5e_save_overlay", "menu_option_header_5e_save_overlay_debug", "option_entry_cycler", { labels = "option_val_off|option_val_debug|option_val_verbose", values = "off|debug|verbose", default = "off" });

	OptionsManager.registerOption2("FIVEE_SAVE_OVERLAY_RESET_ON_TURN_END", false, "menu_option_header_5e_save_overlay", "menu_option_header_5e_save_overlay_reset_on_turn_end", "option_entry_cycler", { labels = "option_val_off|option_val_on", values = "off|on", default = "on" });

	OptionsManager.registerOption2("FIVEE_SAVE_OVERLAY_VISIBILITY", false, "menu_option_header_5e_save_overlay", "menu_option_header_5e_save_overlay_visibility", "option_entry_cycler", { labels = "option_val_gm_only|option_val_everyone", values = "gm|all", default = "gm" });
end
