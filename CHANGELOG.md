# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.0] - 2021-03-03
### Added
- Extension announcement text
- Added /clearSaveOverlays command to clear save overlays for everyone
- Menu option to clear save overlays automatically on combat turn end
- Menu option as to whether or not save overlays are visible to everyone or GM only

### Changed
- Replaced User.isHost() with Session.IsHost

## [1.0.1] - 2021-02-23
### Changed
- Saves from party sheet not working properly

## [1.0.0] - 2021-02-23
### Added
- Overlays based on save results
